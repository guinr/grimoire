import { Request, Response } from 'express'
import UserCards from '../schemas/userCards'

class UserCardsController {
  public async find (req: Request, res: Response): Promise<Response> {
    const userId = req.params.id
    const userCards = await UserCards.findOne({ user: userId }).populate('cards')

    return res.json(userCards)
  }

  public async create (req: Request, res: Response): Promise<Response> {
    const userCards = await UserCards.create(req.body)
    return res.json(userCards)
  }

  public async remove (req: Request, res: Response): Promise<void> {
    const userCardsId = req.params.id
    await UserCards.findByIdAndRemove(userCardsId)
    res.send()
  }
}

export default new UserCardsController()
