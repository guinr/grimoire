import { Request, Response } from 'express'
import Deck from '../schemas/deck'
import UserCards from '../schemas/userCards'

class ComparisonController {
  public async find (req: Request, res: Response): Promise<Response> {
    const deckId = req.params.deckId
    const deck = await Deck.findById(deckId)

    const userId = req.params.userId
    const userCards = await UserCards.findOne({ user: userId })

    return res.json({ deck, userCards })
  }
}

export default new ComparisonController()
