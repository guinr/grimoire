import { Request, Response } from 'express'
import Card from '../schemas/card'

class CardController {
  public async find (req: Request, res: Response): Promise<Response> {
    const cards = await Card.find()
    return res.json(cards)
  }

  public async create (req: Request, res: Response): Promise<Response> {
    const card = await Card.create(req.body)
    return res.json(card)
  }
}

export default new CardController()
