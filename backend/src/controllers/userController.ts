import { Request, Response } from 'express'
import User from '../schemas/user'

class UserController {
  public async login (req: Request, res: Response): Promise<Response> {
    const { email, password } = req.body
    const user = await User.findOne({ email: email, password: password})

    if (Array.isArray(user) && user.length === 0) {
      return res.sendStatus(404)
    }

    return res.json(user?._id)
  }

  public async create (req: Request, res: Response): Promise<Response> {
    const user = await User.create(req.body)
    return res.json(user)
  }
}

export default new UserController()
