import { Request, Response } from 'express'
import Deck from '../schemas/deck'

class DeckController {
  public async find (req: Request, res: Response): Promise<Response> {
    const userId = req.params.id
    const decks = await Deck.find({ user: userId }).populate('cards')
    return res.json(decks)
  }

  public async create (req: Request, res: Response): Promise<Response> {
    const deck = await Deck.create(req.body)
    return res.json(deck)
  }

  public async remove (req: Request, res: Response): Promise<void> {
    const deckId = req.params.id
    await Deck.findByIdAndRemove(deckId)
    res.send()
  }
}

export default new DeckController()
