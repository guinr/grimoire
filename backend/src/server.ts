import app from './app'

app.listen(process.env.PORT || 7777, () => {
  console.log('⚡️ Server listening on http://localhost:7777')
})
