import { Schema, model, Document } from 'mongoose'

interface UserCardsModel extends Document {
    user?: string;
    cards?: string[];
}

const UserCardsSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  cards: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Card'
    }
  ]
}, {
  timestamps: true
})

export default model<UserCardsModel>('UserCards', UserCardsSchema)
