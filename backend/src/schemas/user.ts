import { Schema, model, Document } from 'mongoose'

interface UserModel extends Document {
    email?: string;
    password?: string;
}

const UserSchema = new Schema({
  email: {
    type: String,
    trim: true,
    required: true,
  },
  password: {
    type: String,
    trim: true,
    required: true
  }
}, {
  timestamps: true
})

export default model<UserModel>('User', UserSchema)
