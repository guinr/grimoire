import { Schema, model, Document } from 'mongoose'

interface DeckModel extends Document {
    name?: string;
    user?: string;
    cards?: string[];
}

const DeckSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  cards: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Card'
    }
  ]
}, {
  timestamps: true
})

export default model<DeckModel>('Deck', DeckSchema)
