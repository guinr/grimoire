import { Schema, model, Document } from 'mongoose'

interface CardModel extends Document {
    name?: string;
}

const CardSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: true
  }
}, {
  timestamps: true
})

export default model<CardModel>('Card', CardSchema)
