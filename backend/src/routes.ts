import { Router } from 'express'
import userController from '@controllers/userController'
import cardsController from '@controllers/cardController'
import deckController from '@controllers/deckController'
import userCardsController from '@controllers/userCardsController'
import comparisonController from '@controllers/comparisonController'

const routes = Router()

routes.post('/users/login', userController.login)
routes.post('/users', userController.create)

routes.get('/cards', cardsController.find)
routes.post('/cards', cardsController.create)

routes.get('/decks/:id', deckController.find)
routes.post('/decks', deckController.create)
routes.delete('/decks/:id', deckController.remove)

routes.get('/userCards/:id', userCardsController.find)
routes.post('/userCards', userCardsController.create)
routes.delete('/userCards/:id', userCardsController.remove)

routes.get('/comparison/:userId/:deckId', comparisonController.find)

export default routes
