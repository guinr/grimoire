import express from 'express'
import routes from './routes'
import mongoose from 'mongoose'
import cors from 'cors'

class App {
  public express: express.Application

  constructor () {
    this.express = express()
    this.middlewares()
    this.database()
    this.routes()
  }

  private middlewares (): void {
    this.express.use(express.json())
    this.express.use(cors())
  }

  private database (): void {
    const pass = 'GqkL1rMHqb0aaJRw'
    mongoose.connect(`mongodb+srv://ts-node:${pass}@cluster0-tgpng.mongodb.net/test?retryWrites=true&w=majority`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      dbName:'test'
    })
  }

  private routes (): void {
    this.express.use(routes)
  }
}

export default new App().express
