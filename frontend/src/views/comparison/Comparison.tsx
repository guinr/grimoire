import React, { useState, useEffect } from 'react'
import api from '../../services/Api'
import './Comparison.css'
import UserDecksModel from '../../models/UserDecksModel'
import { Link } from 'react-router-dom'
import UserCardsModel from '../../models/UserCardsModel'

function Comparison() {

  const userId = localStorage.getItem('userId')
  const [userCards, setUserCards] = useState<UserCardsModel>()
  const [userDecks, setUserDecks] = useState<UserDecksModel[]>()
  const [userDeckCompared, setUserDeckCompared] = useState<UserDecksModel>()

  useEffect(findUserCards, [userId])
  useEffect(findUserDecks, [userId])

  function findUserCards() {
    if (userId) {
      api.get(`userCards/${userId}`)
        .then((response) => {
          setUserCards(response.data || undefined)
        })
    }
  }

  function findUserDecks() {
    if (userId) {
      api.get(`decks/${userId}`)
        .then((response) => {
          setUserDecks(response.data || undefined)
        })
    }
  }

  function compareDeck(deck: UserDecksModel) {
    let userCardsCompare = Object.assign({}, userCards)
    deck.cards.forEach(deckCard => {
      if (userCardsCompare.cards.find((card => card._id === deckCard._id))) {
        deckCard.owned = true
        userCardsCompare.cards = userCardsCompare.cards.filter((card) => card._id !== deckCard._id)
      }
    })
    setUserDeckCompared(deck)
  }

  function renderDeckNames() {
    return userDecks ? (
      <div className="decks-to-compare">
        {userDecks?.map(deck => {
          return (
            <div key={deck._id} className="card-deck">
              <span>{deck.name}</span>
              <button onClick={() => {compareDeck(deck)}}>Check Cards</button>
            </div>
          )
        })}
      </div>
    ) : undefined
  }

  function renderComparedDeck() {
    return userDeckCompared ? (
      <div className="compared-deck">
        <h1 className="header-card-name">{userDeckCompared.name}</h1>
        {
          userDeckCompared.cards.map((card, index) => {
            return (
              <div key={index + card._id} className="compared-card">
                <span className="compared-card-index">{index}</span>
                <span className="compared-card-name">{card.name}</span>
                <div className="compared-card-status">
                  <span>Owned:</span>
                  <span className={card.owned ? 'own' : 'not-own'}>{card.owned ? "own" : "not owned"}</span>
                </div>
              </div>
            )
          })
        }
      </div>
    ) : undefined
  }

  return (
    <div className="comparison">
      <div className="user-decks-nav">
        <Link to="/dashboard">
          <button>Voltar</button>
        </Link>
        <div>
          <h1>User Decks</h1>
          <h4>Select a deck and check if you have the required cards</h4>
        </div>
        <div></div>
      </div>
      {renderDeckNames()}
      {renderComparedDeck()}
    </div>
  )
}

export default Comparison