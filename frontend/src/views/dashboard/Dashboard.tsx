import React from 'react'
import './Dashboard.css'
import { Link } from 'react-router-dom'

function Dashboard() {

  return (
    <div className="dashboard">
      <h1>Dashboard</h1>
      <nav>
        <Link to="/userCards">
          <div>User Cards</div>
        </Link>
        <Link to="/comparison">
          <div>Comparison</div>
        </Link>
        <Link to="/decks">
          <div>Decks</div>
        </Link>
      </nav>
    </div>
  )
}

export default Dashboard