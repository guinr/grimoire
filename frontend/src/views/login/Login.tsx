import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import history from '../../services/History'
import api from '../../services/Api'
import './Login.css'

function Login() {

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [loginFailed, setLoginFailed] = useState<Boolean | undefined>()

  function login(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault()
    setLoginFailed(false)
    api.post('users/login', {
      email,
      password
    }).then((response) => {
      if (!response.data || response.data === '') {
        setLoginFailed(true)
      } else {
        localStorage.setItem('userId', response.data)
        history.push('/dashboard')
      }
    }).catch(() => {
      setLoginFailed(true)
    })
  }

  function renderMessageLoginFailed() {
    return (loginFailed !== undefined && loginFailed) ? <h4>Usuário ou senha inválido</h4> : undefined
  }

  return (
    <div className="page-wrap">
      <form onSubmit={login}>
        <h1>Login</h1>
        {renderMessageLoginFailed()}
        <label htmlFor="email">E-mail</label>
        <input id="email" onChange={e => setEmail(e.target.value)}/>
        <label htmlFor="password">Password</label>
        <input id="password" type="password" onChange={e => setPassword(e.target.value)}/>
        <button type="submit">Enter</button>
        <Link to="/register">
          <button>Register</button>
        </Link>
      </form>
    </div>
  )
}

export default Login