import React, { useState, useEffect, ChangeEvent } from 'react'
import { Link } from 'react-router-dom';
import api from '../../services/Api'
import UserCardsModel from '../../models/UserCardsModel';
import csv from 'csvtojson'
import './UserCards.css'
import CardModel from '../../models/CardModel';

function UserCards() {

  const userId = localStorage.getItem('userId') || undefined
  const [userCards, setUserCards] = useState<UserCardsModel>()
  const [file, setFile] = useState<string | null>()
  
  useEffect(findUserCards, [userId])

  function findUserCards() {
    if (userId) {
      api.get(`userCards/${userId}`)
        .then((response) => {
          setUserCards(response.data || undefined)
        })
    }
  }

  function readFile(event: ChangeEvent<HTMLInputElement>) {
    const file = event.target.files?.item(0)
    if (file) {
      const reader = new FileReader()
      reader.onload = async (event) => {
        setFile(event.target?.result?.toString())
      }

      reader.readAsText(file)
    }
  }

  function importCards() {
    if (file) {
      csv({ noheader: false, delimiter: ';' })
        .fromString(file)
        .then((csvCardsAsJson: CardModel[]) => {
          const userCards: UserCardsModel = {
            user: {
              _id: userId
            },
            cards: csvCardsAsJson
          }
          api.post('userCards', userCards)
            .then(() => {
              findUserCards()
            })
        })
    }
  }

  function deleteCards() {
    if (userCards) {
      api.delete(`userCards/${userCards._id}`)
        .then(() => {
          setUserCards(undefined)
        })
    }
  }

  function renderCardsUpload() {
    return !userCards ? (
      <div>
        <input type="file" onChange={e => readFile(e)}></input>
        <button onClick={importCards}>Import Cards</button>
      </div>
    ) : undefined
  }

  function renderCardList() {
    return userCards ? (
      <div className="card-list">
        {userCards?.cards.map((card, index) => {
          return (
            <div key={card._id} className="user-card">
              <span>{index} - </span>
              <span>{card.name}</span>
            </div>
          )
        })}
      </div>
    ) : undefined
  }

  return (
    <div className="user-cards">
      <div className="user-cards-nav">
        <Link to="/dashboard">
          <button>Voltar</button>
        </Link>
        <div>
          <h1>User Cards</h1>
        </div>
        <button onClick={deleteCards} disabled={!userCards}>Delete Cards</button>
      </div>
      {renderCardsUpload()}
      {renderCardList()}
    </div>
  )
}


export default UserCards