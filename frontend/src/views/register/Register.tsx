import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import api from '../../services/Api'
import history from '../../services/History'
import './Register.css'

function Register() {

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  function register(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault()
    api.post('users', {
      email,
      password
    }).then(() => {
      history.push('/login')
    })
  }

  return (
    <div className="page-wrap">
      <form onSubmit={register}>
        <h1>Register</h1>
        <label htmlFor="email">E-mail</label>
        <input id="email" name="email" onChange={e => setEmail(e.target.value)}/>
        <label htmlFor="password">Password</label>
        <input id="password" name="password" type="password" onChange={e => setPassword(e.target.value)}/>
        <button type="submit">Register</button>
        <Link to="/login">
          <button>Back to login</button>
        </Link>
      </form>
    </div>
  )
}

export default Register