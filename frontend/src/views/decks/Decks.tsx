import React, { useState, useEffect } from 'react'
import api from '../../services/Api'
import { Link } from 'react-router-dom'
import UserDecksModel from '../../models/UserDecksModel'
import './Decks.css'

function Decks() {

  const userId = localStorage.getItem('userId')
  const [userDecks, setUserDecks] = useState<UserDecksModel[]>()

  useEffect(findUserDecks, [userId])

  function findUserDecks() {
    if (userId) {
      api.get(`decks/${userId}`)
        .then((response) => {
          setUserDecks(response.data || undefined)
        })
    }
  }

  function renderDeckList() {
    return userDecks ? (
      <div>
        {userDecks?.map(deck => {
          return (
            <div key={deck?._id} className="user-deck">
              <h3>{deck?.name}</h3>
              <div className="user-deck-card-list">
                {deck?.cards.map((card, index) => {
                  return (
                    <div key={index + card._id} className="user-deck-card">
                      <span>{index} - </span>
                      <span key={card._id}>{card.name}</span>
                    </div>
                  )
                })}
              </div>
            </div>
          )
        })}
      </div>
    ) : undefined
  }

  function deleteDecks() {

  }

  return (
    <div className="user-decks">
      <div className="user-decks-nav">
        <Link to="/dashboard">
          <button>Voltar</button>
        </Link>
        <div>
          <h1>User Decks</h1>
        </div>
        <button onClick={deleteDecks} disabled={!userDecks}>Delete Decks</button>
      </div>
      {renderDeckList()}
    </div>
  )
}

export default Decks