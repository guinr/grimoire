import React from 'react';
import { Router, Switch, Route } from 'react-router-dom'
import './App.css';
import Login from './views/login/Login'
import Register from './views/register/Register';
import Dashboard from './views/dashboard/Dashboard';
import UserCards from './views/userCards/UserCards';
import Comparison from './views/comparison/Comparison';
import Decks from './views/decks/Decks';
import history from './services/History';

function App() {
  return (
    <div className="App">
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={Login}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/dashboard" component={Dashboard}/>
          <Route exact path="/userCards" component={UserCards}/>
          <Route exact path="/comparison" component={Comparison}/>
          <Route exact path="/decks" component={Decks}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
