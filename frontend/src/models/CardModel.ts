export default interface CardModel {
  _id: string,
  name?: string,
  owned?: boolean
}