import UserModel from "./UserModel";
import CardModel from "./CardModel";

export default interface UserDecksModel {
  _id?: string,
  name: string,
  user: UserModel,
  cards: CardModel[]
}