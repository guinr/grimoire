import UserModel from "./UserModel";
import CardModel from "./CardModel";

export default interface UserCardsModel {
  _id?: string,
  user: UserModel,
  cards: CardModel[]
}